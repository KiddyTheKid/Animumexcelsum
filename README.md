# Animum Excelsum

This platform is aimed to provide a safe and easy way to publish your own podcasts or audio files to the public. 

`To create an admin user, use PHP's password_hash("pass", PASSWORD_DEFAULT); to generate your pass and insert in manually on usuarios table.`

> Written in PHP using Codeigniter Framework 3
> Tags releases using Semantic Versioning 2.0.0. More at https://semver.org/