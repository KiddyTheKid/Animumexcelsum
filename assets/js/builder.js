function crearPensamiento(contenedor, pens) {
    var divPrincipal = document.createElement("div");
    divPrincipal.className = "t-card";

    var h3Titulo = document.createElement("h3");
    h3Titulo.appendChild(document.createTextNode(pens.titulo));

    var smallDate = document.createElement("small");
    smallDate.appendChild(document.createTextNode(pens.fecha));

    var pPensamiento = document.createElement("p");
    pPensamiento.appendChild(document.createTextNode(pens.pensamiento));

    divPrincipal.appendChild(h3Titulo);
    divPrincipal.appendChild(smallDate);
    divPrincipal.appendChild(pPensamiento);

    document.getElementById(contenedor).appendChild(divPrincipal);
}
