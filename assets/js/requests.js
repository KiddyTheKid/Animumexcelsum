var hostNow = window.location.host;
var formPensamiento, closeFormPensamiento, cuerpoDoc;

function pedirPensamientos() {
    var ruta = `http://localhost/pensamientosapi/Com/Receptor/Pensamientos/getPens.php?token=SuperClave`;
    $.ajax({
        type: 'POST',
        url: ruta,
        success: function (data) {
            data.map(function (pen) {
                crearPensamiento("contents", pen);
            });
        }
    });
}

function mostrarFormPensamiento() {
    if (!formPensamiento.show) {
        formPensamiento.DOM.setAttribute("style", "left: 50%;");
        cuerpoDoc.className = "hideme";
    } else {
        formPensamiento.DOM.setAttribute("style", "left: -50%; visibility: hidden;");
        cuerpoDoc.className = "";
    }
    formPensamiento.show = !formPensamiento.show;
}

window.onload = function () {
    cuerpoDoc = document.getElementsByTagName("body")[0];
    pedirPensamientos();
    closeFormPensamiento = document.getElementsByClassName("exit")[0].addEventListener("click", function () {
        mostrarFormPensamiento();
    });

    formPensamiento = {
        DOM: document.getElementById("form-pensamiento"),
        show: false
    };
}