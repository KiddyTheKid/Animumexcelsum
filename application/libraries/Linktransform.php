<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Linktransform {

    public function obtenerbase64($codigo) {
        $filtro1 = str_replace("-", "+", $codigo);
        $filtro2 = str_replace("_", "/", $filtro1);
        $filtro3 = str_replace(".", "=", $filtro2);
        return $filtro3;
    }

    public function obtenerurldebase64($hash) {
        $filtro1 = str_replace("+", "-", $hash);
        $filtro2 = str_replace("/", "_", $filtro1);
        $filtro3 = str_replace("=", ".", $filtro2);
        return $filtro3;
    }

}
