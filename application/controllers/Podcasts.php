<?php

defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Podcasts
 *
 * @author pcost8300
 */
class Podcasts extends CI_Controller {

    public function nuevopodcast($error = '') {
        if (!$this->session->adminlogeado) {
            redirect('animum');
            return;
        }
        doctype('html');
        $this->load->view('admin/include/head');
        $this->load->view('admin/include/bodyopen');
        $this->load->view('admin/templates/navbar');
        $this->load->view('admin/templates/aside');
        $this->load->view('admin/forms/nuevopodcast', ['error' => $error]);
        $this->load->view('admin/templates/footer');
        $this->load->view('admin/include/footer');
        $this->load->view('admin/include/bodyclose');
    }

    public function editarpodcast($podcast_id) {
        if (!$this->session->adminlogeado) {
            redirect('animum');
            return;
        }
        $podcast = $this->get_podcast($podcast_id);
        doctype('html');
        $this->load->view('admin/include/head');
        $this->load->view('admin/include/bodyopen');
        $this->load->view('admin/templates/navbar');
        $this->load->view('admin/templates/aside');
        $this->load->view('admin/forms/editarpodcast', $podcast);
        $this->load->view('admin/templates/footer');
        $this->load->view('admin/include/footer');
        $this->load->view('admin/include/bodyclose');
    }

    public function eliminarpodcast($podcast_id) {
        if (!$this->session->adminlogeado) {
            redirect('animum');
            return;
        }
        $podcast = $this->get_podcast($podcast_id);
        doctype('html');
        $this->load->view('admin/include/head');
        $this->load->view('admin/include/bodyopen');
        $this->load->view('admin/templates/navbar');
        $this->load->view('admin/templates/aside');
        $this->load->view('admin/forms/eliminarpodcast', $podcast);
        $this->load->view('admin/templates/footer');
        $this->load->view('admin/include/footer');
        $this->load->view('admin/include/bodyclose');
    }

    public function crear() {
        if (!$this->session->adminlogeado) {
            redirect('animum');
            return;
        }
        $subida_portada = $this->subirportada();
        $subida_podcast = $this->subirpodcast();
        if (!$subida_podcast['success']) {
            $this->nuevopodcast($subida_podcast['error']);
            return;
        }
        if (!$subida_portada['success']) {
            $this->nuevopodcast($subida_portada['error']);
            return;
        }
        $this->registrarpodcast($subida_podcast, $subida_portada);
        redirect('animum/podcasts');
    }

    public function escuchar($id) {
        echo doctype('html');
        $this->load->view('include/head');
        $this->load->view('include/header', ['volver' => '']);
        $this->load->view('templates/podcast', $this->get_podcast($id));
    }

    private function subirpodcast() {
        if (!file_exists('./uploads/podcasts/')) {
            mkdir('./uploads/podcasts/', 777, true);
        }
        $config['upload_path'] = './uploads/podcasts/';
        $config['remove_spaces'] = TRUE;
        $config['encrypt_name'] = TRUE;
        $config['allowed_types'] = 'mp3';
        $this->upload->initialize($config, TRUE);
        if ($this->upload->do_upload('podcast')) {
            return ['success' => true, 'data' => $this->upload->data()];
        }
        return ['success' => false, 'error' => $this->upload->display_errors()];
    }

    private function subirportada() {
        if (!file_exists('./uploads/portadas/')) {
            mkdir('./uploads/portadas/', 777, true);
        }
        $config['upload_path'] = './uploads/portadas/';
        $config['remove_spaces'] = TRUE;
        $config['encrypt_name'] = TRUE;
        $config['allowed_types'] = 'jpg|jpeg|png';
        $this->upload->initialize($config, TRUE);
        if ($this->upload->do_upload('portada')) {
            return ['success' => true, 'data' => $this->upload->data()];
        }
        return ['success' => false, 'error' => $this->upload->display_errors()];
    }

    private function registrarpodcast($podcast, $portada) {
        $this->load->model('podcasts_model');
        $this->podcasts_model->titulo = $this->input->post('titulo');
        $this->podcasts_model->descripcion = $this->input->post('descripcion');
        $this->podcasts_model->archivo = $podcast['data']['file_name'];
        $this->podcasts_model->portada = $portada['data']['file_name'];
        $this->podcasts_model->crear();
    }

    private function get_podcast($id) {
        $this->load->model('podcasts_model');
        return ['podcast' => $this->podcasts_model->get_podcast($id)];
    }

}
