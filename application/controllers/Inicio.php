<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Inicio extends CI_Controller {

    public function index() {
        echo doctype('html');
        $this->load->view('include/head');
        $this->load->view('include/header');
        $this->load->view('templates/podcasts', $this->obtener_podcasts());
    }

    private function obtener_podcasts() {
        $this->load->model('podcasts_model');
        $config = [];
        $config['base_url'] = base_url() . 'inicio';
        $config['total_rows'] = $this->podcasts_model->get_count();
        $config['per_page'] = 5;
        $config['uri_segment'] = 2;
        $config['num_links'] = 1;
        $config['first_link'] = 'Primero';
        $config['last_link'] = 'Ultimo';
        $config['next_link'] = '>';
        $config['prev_link'] = '<';
        $this->pagination->initialize($config);
        $page = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0;

        $data = [];
        $data['links'] = $this->pagination->create_links();
        $data['podcasts'] = $this->podcasts_model->get_podcasts_pg($config['per_page'], $page)->result_array();
        return $data;
    }

}
