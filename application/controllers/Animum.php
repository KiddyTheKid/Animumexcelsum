<?php

defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Animum
 *
 * @author pcost8300
 */
class Animum extends CI_Controller {

    public function index() {
        if (!$this->session->adminlogeado) {
            $this->mostrar_login();
            return;
        }
        doctype('html');
        $this->load->view('admin/include/head');
        $this->load->view('admin/include/bodyopen');
        $this->load->view('admin/templates/navbar');
        $this->load->view('admin/templates/aside');
        $this->load->view('admin/templates/starter');
        $this->load->view('admin/templates/footer');
        $this->load->view('admin/include/footer');
        $this->load->view('admin/include/bodyclose');
    }

    public function ingresar() {
        $usuario = $this->get_usuario();
        if (password_verify($this->input->post('password'), $usuario['pwd'])) {
            $this->session->adminlogeado = true;
            $this->session->usuario = $usuario;
        }
        redirect('animum');
    }

    public function cerrar_sesion() {
        $this->session->sess_destroy();
        redirect('animum');
    }

    public function podcasts() {
        if (!$this->session->adminlogeado) {
            redirect('animum');
            return;
        }
        doctype('html');
        $this->load->view('admin/include/head');
        $this->load->view('admin/include/bodyopen');
        $this->load->view('admin/templates/navbar');
        $this->load->view('admin/templates/aside');
        $this->load->view('admin/templates/podcasts', $this->get_podcasts());
        $this->load->view('admin/templates/footer');
        $this->load->view('admin/include/footer');
        $this->load->view('admin/include/bodyclose');
    }

    public function usuarios() {
        if (!$this->session->adminlogeado) {
            redirect('animum');
            return;
        }
        doctype('html');
        $this->load->view('admin/include/head');
        $this->load->view('admin/include/bodyopen');
        $this->load->view('admin/templates/navbar');
        $this->load->view('admin/templates/aside');
        $this->load->view('admin/templates/starter');
        $this->load->view('admin/templates/footer');
        $this->load->view('admin/include/footer');
        $this->load->view('admin/include/bodyclose');
    }

    private function get_usuario() {
        $this->load->model('usuario_model');
        return $this->usuario_model->get_usuario_por_correo($this->input->post('correo'));
    }

    private function get_podcasts() {
        $this->load->model('podcasts_model');
        return ['podcasts' => $this->podcasts_model->get_podcasts()->result_array()];
    }

    private function mostrar_login() {
        doctype('html');
        $this->load->view('admin/include/head');
        $this->load->view('admin/include/bodyopen');
        $this->load->view('admin/forms/login');
        $this->load->view('admin/include/footer');
        $this->load->view('admin/include/bodyclose');
    }

}
