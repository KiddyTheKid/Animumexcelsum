<?php

defined('BASEPATH') OR exit('No direct script access allowed');

use chriskacerguis\RestServer\RestController;

class Apipodcasts extends RestController {

    function __construct() {
        parent::__construct();
    }

    public function podcasts_get() {
        $id = $this->get('id');
        header("Access-Control-Allow-Origin: *");
        $this->load->model('podcasts_model');
        if ($id !== null) {
            $podcast = $this->podcasts_model->get_podcast($id);
            $this->response($podcast, 200);
            return;
        }
        $podcasts = $this->podcasts_model->get_podcasts()->result_array();
        $this->response($podcasts, 200);
    }

}
