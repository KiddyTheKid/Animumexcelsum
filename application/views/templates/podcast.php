<script src="<?= base_url() ?>assets/node_modules/howler/dist/howler.min.js" type="text/javascript"></script>
<script src="<?= base_url() ?>assets/node_modules/jquery/dist/jquery.min.js" type="text/javascript"></script>
<script src="<?= base_url() ?>assets/node_modules/animejs/lib/anime.min.js" type="text/javascript"></script>
<section id="contents">
    <figure class="listen" data-audio="<?= base_url() ?>uploads/podcasts/<?= $podcast['archivo'] ?>">
        <img src="<?= base_url() ?>uploads/portadas/<?= $podcast['portada'] ?>">
        <div class="pod-controls" style="display: none;">
            <span><?= $podcast['titulo'] ?></span>
            <img src="<?= base_url() ?>assets/images/play.svg">
            <img src="<?= base_url() ?>assets/images/pause.svg">
            <img src="<?= base_url() ?>assets/images/stop.svg">
            <div class="location">
                controlex
            </div>
        </div>
        <div>
        </div>
    </figure>
</section>
<script>
    var figurita = $('figure')[0];
    var duracion = 0;
    var reproductor = new Howl({
        src: [figurita.dataset.audio],
        html5: true
    });
    reproductor.once('load', function () {
        duracion = reproductor.duration();
        $('.pod-controls').removeAttr('style');
    });
    $('img[src*=play]').on('click', function (e) {
        if (reproductor.playing()) {
            return;
        }
        anime({
            targets: e.currentTarget,
            scale: [1.1, 1]
        });
        reproductor.play();
    });
    $('img[src*=pause]').on('click', function (e) {
        anime({
            targets: e.currentTarget,
            scale: [1.1, 1]
        });
        reproductor.pause();
    });
    $('img[src*=stop]').on('click', function (e) {
        anime({
            targets: e.currentTarget,
            scale: [1.1, 1]
        });
        reproductor.stop();
    });
</script>