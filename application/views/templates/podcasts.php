<section id="contents">
    <?php foreach ($podcasts as $podcast): ?>
        <div class="podcast">
            <a href="<?= base_url('index.php/podcasts/escuchar/') ?><?= $podcast['id'] ?>">
                <img class="img-podcast" src="<?= base_url() ?>uploads/portadas/<?= $podcast['portada'] ?>">
                <div data-audio="<?= base_url() ?>uploads/podcasts/<?= $podcast['archivo'] ?>" class="pod-controls">
                    <h4><?= $podcast['titulo'] ?></h4>
                    <p><?= $podcast['descripcion'] ?></p>
                </div>
            </a>
        </div>
    <?php endforeach; ?>
</section>