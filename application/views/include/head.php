<?php
define('PLAT_VERSION', '1.8.6')
?>
<head>
    <link rel="stylesheet" href="<?= base_url() ?>assets/node_modules/normalize.css/normalize.css"/>
    <link rel="stylesheet" href="<?= base_url() ?>assets/css/estilo.css?v=<?= PLAT_VERSION ?>"/>
    <link rel="shortcut icon" href="<?= base_url() ?>assets/images/ico.jpg"/>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta name="title" content="Animum Excelsum">
    <meta name="description" content="Podcasts acerca de problemas que actualmente ocurren, pero más que eso, una reflexión. Puede que un poco fatalista pero reales al fin y al cabo.">
    <meta name="keywords" content="podcasts,podcast,pensamiento,pensamientos,filosofía,filosofia,reflexión,reflexion,reflexiones,trabajo,explotación,explotacion, realidad, real,actualidad,problemas">
    <meta name="robots" content="index, follow">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="language" content="Spanish">
    <meta name="revisit-after" content="10 days">
    <meta name="author" content="Pedro Acosta">

    <meta property="og:title" content="Animum Excelsum"/>
    <meta property="og:image" content="<?= base_url() ?>assets/images/ico.jpg"/>
    <meta property="og:url" content="<?= base_url() ?>"/>
    <meta property="og:description" content="Podcasts acerca de problemas que actualmente ocurren, pero más que eso, una reflexión. Puede que un poco fatalista pero reales al fin y al cabo."/>
    <title>Animum Excelsum</title>
</head>
<body>