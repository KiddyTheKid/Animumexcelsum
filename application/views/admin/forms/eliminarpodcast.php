<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$input_titulo = [
    'name' => 'titulo',
    'class' => 'form-control',
    'type' => 'text',
    'required' => 'required',
    'disabled' => 'disabled',
    'value' => $podcast['titulo']
];

$input_descripcion = [
    'name' => 'descripcion',
    'class' => 'form-control',
    'type' => 'text',
    'required' => 'required',
    'disabled' => 'disabled',
    'value' => $podcast['descripcion']
];

$input_podcast = [
    'name' => 'podcast',
    'class' => 'form-control',
    'type' => 'file',
    'disabled' => 'disabled',
    'required' => 'required'
];

$input_portada = [
    'name' => 'portada',
    'class' => 'form-control',
    'disabled' => 'disabled',
    'type' => 'file'
];

$button_submit = [
    'value' => 'Eliminar',
    'class' => 'btn btn-primary'
];
?>
<div class="content-wrapper">
    <section class="content">
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">Editar podcast</h3>
            </div>
            <div class="card-body">
                <?= form_open_multipart('podcasts/eliminar/' . $podcast['id']) ?>
                <div class="form-group">
                    <?= form_label('Titulo') ?>
                    <?= form_input($input_titulo) ?>
                </div>
                <div class="form-group">
                    <?= form_label('Descripción') ?>
                    <?= form_textarea($input_descripcion) ?>
                </div>
                <div class="form-group">
                    <?= form_label('Podcast') ?>
                    <?= form_input($input_podcast) ?>
                </div>
                <div class="form-group">
                    <?= form_label('Portada') ?>
                    <?= form_input($input_portada) ?>
                </div>
                <?= form_submit($button_submit) ?>
                <?= anchor('animum/podcasts', 'Volver', ['class' => 'btn btn-danger']) ?>
                <?= form_close() ?>
            </div>
        </div>
    </section>
</div>