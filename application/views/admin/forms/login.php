<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$input_usuario = [
    'name' => 'correo',
    'class' => 'form-control',
    'type' => 'email',
    'required' => 'required'
];

$input_password = [
    'name' => 'password',
    'class' => 'form-control',
    'type' => 'password',
    'required' => 'required'
];

$button_login = [
    'value' => 'Ingresar',
    'class' => 'btn btn-primary'
];
?>
<style>
    html, body {
        display: block;
        position: relative;
        height: 100% !important;
    }
    section * {
        display: block;
        position: relative;
    }
    .card {
        display: block;
        width: 90%;
        position: absolute;
        top: 50%;
        left: 50%;
        transform: translate(-50%, -50%);
    }
</style>
<div class="card card-primary">
    <div class="card-header">
        <h3 class="card-title">Ingreso administrador</h3>
    </div>
    <?= form_open('animum/ingresar', ['role' => 'form']) ?>
    <div class="card-body">
        <div class="form-group">
            <?= form_label('Correo') ?>
            <?= form_input($input_usuario) ?>
        </div>
        <div class="form-group">
            <?= form_label('Contraseña') ?>
            <?= form_password($input_password) ?>
        </div>
    </div>
    <div class="card-footer">
        <?= form_submit($button_login) ?>
    </div>
    <?= form_close() ?>
</div>