<!-- Main Sidebar Container -->
<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="index3.html" class="brand-link">
        <img src="<?= base_url() ?>adminassets/img/AdminLTELogo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
             style="opacity: .8">
        <span class="brand-text font-weight-light">dministrador</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar user panel (optional) -->
        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
            <div class="info">
                <a href="#" class="d-block"><?= $this->session->usuario['nombres'] . ' ' . $this->session->usuario['apellidos'] ?></a>
            </div>
        </div>

        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <li class="nav-item">
                    <?= anchor('animum/podcasts', '<i class="nav-icon fas fa-podcast"></i><p> Podcasts</p>', ['class' => 'nav-link']) ?>
                </li>
                <li class="nav-item">
                    <?= anchor('animum/usuarios', '<i class="nav-icon fas fa-users"></i><p> Usuarios</p>', ['class' => 'nav-link']) ?>
                </li>
                <li class="nav-item">
                    <?= anchor('animum/cerrar_sesion', '<i class="nav-icon fas fa-times-circle"></i><p> Cerrar sesión</p>', ['class' => 'nav-link']) ?>
                </li>
            </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>