<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<style>
    .columna-centrada {
        text-align: center;
    }
    .columna-centrada * {
        vertical-align: middle;
    }
    .dropdown-menu li a {
        display: block;
        padding: 3px 20px;
        clear: both;
        font-weight: 400;
        line-height: 1.42857143;
        white-space: nowrap;
        color: #777;
    }
    .dropdown-menu li a:hover {
        background-color: #e1e3e9;
        color: #333;
        text-decoration: none;
    }
</style>
<div class="content-wrapper">
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Podcasts</h1>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <?= anchor('podcasts/nuevopodcast', 'Subir nuevo', ['class' => 'btn btn-primary']) ?>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <div id="example1_wrapper" class="dataTables_wrapper dt-bootstrap4">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="dataTables_length">
                                        <table class="table table-hover table-bordered" id="example2">
                                            <thead>
                                                <tr>
                                                    <th>*</th>
                                                    <th>Titulo</th>
                                                    <th>Descripción</th>
                                                    <th>Miniatura</th>
                                                    <th>Podcast</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php foreach ($podcasts as $podcast): ?>
                                                    <tr>
                                                        <td>
                                                            <div class="btn-group">
                                                                <button type="button" class="btn btn-default">*</button>
                                                                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                                                    <span class="caret"></span>
                                                                    <span class="sr-only">Toggle Dropdown</span>
                                                                </button>
                                                                <ul class="dropdown-menu" role="menu">
                                                                    <li><?= anchor('podcasts/editarpodcast/' . $podcast['id'], 'Editar') ?></li>
                                                                    <li class="divider"></li>
                                                                    <li><?= anchor('podcasts/eliminarpodcast/' . $podcast['id'], 'Eliminar') ?></li>
                                                                </ul>
                                                            </div>
                                                        </td>
                                                        <td><?= $podcast['titulo'] ?></td>
                                                        <td><?= $podcast['descripcion'] ?></td>
                                                        <td class="columna-centrada">
                                                            <img height="120" src="<?= base_url() ?>uploads/portadas/<?= $podcast['portada'] ?>">
                                                        </td>
                                                        <td class="columna-centrada">
                                                            <audio controls="">
                                                                <source src="<?= base_url() ?>uploads/podcasts/<?= $podcast['archivo'] ?>">       
                                                            </audio>
                                                        </td>
                                                    </tr>
                                                <?php endforeach; ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <!-- /.card-body -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<script>
    lastExec = function () {
        $('#example2').DataTable({
            "paging": true,
            "lengthChange": false,
            "searching": false,
            "ordering": true,
            "info": true,
            "autoWidth": false,
        });
    };
</script>