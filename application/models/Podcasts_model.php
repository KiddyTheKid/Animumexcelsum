<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Podcasts_model
 *
 * @author pcost8300
 */
class Podcasts_model extends CI_Model {

    public $titulo;
    public $descripcion;
    public $portada;
    public $archivo;
    public $activo;

    public function crear() {
        $this->activo = true;
        $this->db->insert('podcasts', (Array) $this);
        return $this->db->insert_id();
    }

    public function editar($id) {
        $this->db->set('titulo', $this->titulo);
        $this->db->set('descripcion', $this->descripcion);
        $this->db->set('portada', $this->portada);
        $this->db->set('archivo', $this->archivo);
        $this->db->where('id', $id);
        $this->db->update('podcasts');
    }

    public function eliminar($id) {
        $this->db->set('activo', false);
        $this->db->where('id', $id);
        $this->db->update('podcasts');
    }

    public function get_podcast($id) {
        $query = $this->db->get_where('podcasts', ['id' => $id]);
        return $query->row_array();
    }

    public function get_podcasts() {
        $this->db->order_by('id DESC');
        $this->db->where('activo', true);
        return $this->db->get('podcasts');
    }

    public function get_podcasts_pg($limite, $inicio) {
        $this->db->limit($limite, $inicio);
        $this->db->order_by('id DESC');
        return $this->db->get('podcasts');
    }

    public function get_count() {
        $this->db->where(['activo' => true]);
        $this->db->order_by('id DESC');
        return $this->db->count_all_results('podcasts');
    }

}
