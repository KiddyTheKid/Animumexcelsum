<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Usuario_model
 *
 * @author pcost8300
 */
class Usuario_model extends CI_Model {

    public $nombres;
    public $apellidos;
    public $correo;
    public $pwd;
    public $esadmin;
    public $activo;

    public function crear() {
        $this->pwd = password_hash($this->pwd, PASSWORD_DEFAULT);
        $this->activo = true;
        $this->db->insert('usuario', (Array) $this);
        return $this->db->insert_id();
    }

    public function editar($id) {
        $this->db->set('nombres', $this->nombres);
        $this->db->set('apellidos', $this->apellidos);
        $this->db->set('correo', $this->correo);
        $this->db->set('esadmin', $this->esadmin);
        $this->db->where('id', $id);
        $this->db->update('usuario');
    }

    public function eliminar($id) {
        $this->db->set('activo', false);
        $this->db->where('id', $id);
        $this->db->update('usuario');
    }

    public function get_usuario($id) {
        $query = $this->db->get_where('usuario', ['id' => $id]);
        return $query->row_array();
    }

    public function get_usuario_por_correo($correo) {
        $query = $this->db->get_where('usuario', [
            'correo' => $correo,
            'activo' => true
        ]);
        return $query->row_array();
    }

}
